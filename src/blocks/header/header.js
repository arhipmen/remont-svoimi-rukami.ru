let $topnav = document.querySelector("#topnav");
let $btnopen = document.querySelector("#btnopen");
let $btnclose = document.querySelector("#btnclose");
$btnopen.addEventListener("click", () => {
	let btnopen = $topnav.classList.toggle("responsive", true);
	if (btnopen){
		$btnopen.classList.add("hide");
		$btnclose.classList.remove("hide");
	}
});
$btnclose.addEventListener("click", () => {
	let btnclose = $topnav.classList.toggle("responsive", false);
	if(!btnclose){
		$btnopen.classList.remove("hide");
		$btnclose.classList.add("hide");
	}
});